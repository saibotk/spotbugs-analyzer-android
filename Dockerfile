FROM registry.gitlab.com/gitlab-org/security-products/analyzers/spotbugs:latest

ENV ANDROID_COMPILE_SDK 29
ENV ANDROID_BUILD_TOOLS 29.0.2
ENV ANDROID_SDK_TOOLS 4333796

ENV ANDROID_HOME=/opt/android-sdk-linux
ENV PATH=$PATH:/opt/android-sdk-linux/platform-tools/

# Install android sdk
RUN mkdir -p /opt/android-sdk-linux && \
    cd /opt && \
    wget --quiet --output-document=android-sdk.zip https://dl.google.com/android/repository/sdk-tools-linux-${ANDROID_SDK_TOOLS}.zip && \
    unzip -d /opt/android-sdk-linux android-sdk.zip && \
    rm -f android-sdk.zip && \
    echo y | /opt/android-sdk-linux/tools/bin/sdkmanager "platforms;android-${ANDROID_COMPILE_SDK}" && \
    echo y | /opt/android-sdk-linux/tools/bin/sdkmanager "platform-tools" && \
    echo y | /opt/android-sdk-linux/tools/bin/sdkmanager "build-tools;${ANDROID_BUILD_TOOLS}" && \
    # temporarily disable checking for EPIPE error and use yes to accept all licenses
    set +o pipefail && \
    yes | android-sdk-linux/tools/bin/sdkmanager --licenses && \
    set -o pipefail
